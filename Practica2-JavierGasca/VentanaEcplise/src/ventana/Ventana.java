package ventana;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JToolBar;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JButton;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import java.awt.SystemColor;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.Color;
import javax.swing.JSpinner;
import javax.swing.JSlider;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JProgressBar;
import java.awt.Toolkit;

public class Ventana extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private JTextField textField_3;
	private JTextField textField_4;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana frame = new Ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Ventana.class.getResource("/Imagenes/chuches (1).jpg")));
		setTitle("Ventana");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 638, 391);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		JMenuItem mntmGuardar = new JMenuItem("Guardar");
		mnArchivo.add(mntmGuardar);
		
		JMenuItem mntmGuardarComo = new JMenuItem("Guardar como...");
		mnArchivo.add(mntmGuardarComo);
		
		JMenu mnEditar = new JMenu("Editar");
		menuBar.add(mnEditar);
		
		JMenu mnAcciones = new JMenu("Acciones");
		mnEditar.add(mnAcciones);
		
		JMenuItem mntmCopiar = new JMenuItem("Copiar");
		mnAcciones.add(mntmCopiar);
		
		JMenuItem mntmCortar = new JMenuItem("Cortar");
		mnAcciones.add(mntmCortar);
		
		JMenuItem mntmPegar = new JMenuItem("Pegar");
		mnAcciones.add(mntmPegar);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBounds(0, 0, 861, 23);
		contentPane.add(toolBar);
		
		JButton btnNewButton = new JButton("Abrir");
		toolBar.add(btnNewButton);
		
		JButton btnEliminar = new JButton("Eliminar");
		toolBar.add(btnEliminar);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 34, 851, 443);
		contentPane.add(tabbedPane);
		
		JPanel COMPRACHUCHERIAS = new JPanel();
		COMPRACHUCHERIAS.setBackground(SystemColor.textHighlight);
		COMPRACHUCHERIAS.setForeground(SystemColor.controlShadow);
		tabbedPane.addTab("Compra Chuerias", null, COMPRACHUCHERIAS, null);
		tabbedPane.setForegroundAt(0, SystemColor.textHighlight);
		tabbedPane.setBackgroundAt(0, SystemColor.desktop);
		COMPRACHUCHERIAS.setLayout(null);
		
		JLabel lblNomber = new JLabel("Nombre :");
		lblNomber.setBounds(10, 11, 56, 14);
		COMPRACHUCHERIAS.add(lblNomber);
		
		JLabel lblDireccin = new JLabel("Tipo :");
		lblDireccin.setBounds(10, 34, 61, 14);
		COMPRACHUCHERIAS.add(lblDireccin);
		
		textField = new JTextField();
		textField.setBounds(65, 8, 86, 20);
		COMPRACHUCHERIAS.add(textField);
		textField.setColumns(10);
		
		JLabel lblApellidos = new JLabel("id :");
		lblApellidos.setBounds(161, 11, 30, 14);
		COMPRACHUCHERIAS.add(lblApellidos);
		
		textField_1 = new JTextField();
		textField_1.setBounds(183, 8, 86, 20);
		COMPRACHUCHERIAS.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblVehiculoPropio = new JLabel("Empaquetado:\r\n");
		lblVehiculoPropio.setBounds(10, 59, 108, 14);
		COMPRACHUCHERIAS.add(lblVehiculoPropio);
		
		JRadioButton rdbtnSi = new JRadioButton("Bolsa\r\n");
		rdbtnSi.setBackground(SystemColor.textHighlight);
		buttonGroup.add(rdbtnSi);
		rdbtnSi.setBounds(99, 55, 66, 23);
		COMPRACHUCHERIAS.add(rdbtnSi);
		
		JRadioButton rdbtnNo = new JRadioButton("Caja\r\n");
		rdbtnNo.setBackground(SystemColor.textHighlight);
		rdbtnNo.setSelected(true);
		buttonGroup.add(rdbtnNo);
		rdbtnNo.setBounds(161, 55, 79, 23);
		COMPRACHUCHERIAS.add(rdbtnNo);
		
		JCheckBox chckbxHombre = new JCheckBox("Estandar");
		chckbxHombre.setBackground(SystemColor.textHighlight);
		buttonGroup_1.add(chckbxHombre);
		chckbxHombre.setBounds(48, 80, 86, 23);
		COMPRACHUCHERIAS.add(chckbxHombre);
		
		JCheckBox chckbxMujer = new JCheckBox("Urgente\r\n");
		chckbxMujer.setBackground(SystemColor.textHighlight);
		buttonGroup_1.add(chckbxMujer);
		chckbxMujer.setBounds(136, 81, 73, 23);
		COMPRACHUCHERIAS.add(chckbxMujer);
		
		JLabel lblSexo = new JLabel("Envio:");
		lblSexo.setBounds(10, 84, 46, 14);
		COMPRACHUCHERIAS.add(lblSexo);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBackground(SystemColor.text);
		scrollPane.setBounds(88, 109, 226, 59);
		COMPRACHUCHERIAS.add(scrollPane);
		
		JTextArea textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		
		JLabel lblDescripcion = new JLabel("Descripci\u00F3n :");
		lblDescripcion.setBounds(10, 110, 79, 14);
		COMPRACHUCHERIAS.add(lblDescripcion);
		
		JButton btnEnviar = new JButton("Enviar");
		btnEnviar.setBounds(10, 179, 89, 23);
		COMPRACHUCHERIAS.add(btnEnviar);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Gominola", "Regaliz", "chicles", "Caramelos", "Frutos secos"}));
		comboBox.setBounds(43, 31, 108, 20);
		COMPRACHUCHERIAS.add(comboBox);
		
		JPanel MusicaInfantil = new JPanel();
		MusicaInfantil.setForeground(SystemColor.controlShadow);
		MusicaInfantil.setBackground(SystemColor.controlShadow);
		tabbedPane.addTab("Musica Infantil", null, MusicaInfantil, null);
		tabbedPane.setForegroundAt(1, SystemColor.desktop);
		tabbedPane.setBackgroundAt(1, SystemColor.controlShadow);
		MusicaInfantil.setLayout(null);
		
		JLabel label = new JLabel("Nombre :");
		label.setBounds(10, 14, 56, 14);
		MusicaInfantil.add(label);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(65, 11, 86, 20);
		MusicaInfantil.add(textField_3);
		
		JLabel lblAo = new JLabel("A\u00F1o :");
		lblAo.setBounds(161, 14, 61, 14);
		MusicaInfantil.add(lblAo);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(195, 11, 86, 20);
		MusicaInfantil.add(textField_4);
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.setBounds(10, 123, 89, 23);
		MusicaInfantil.add(btnBuscar);
		
		JSlider slider = new JSlider();
		slider.setValue(1);
		slider.setBackground(SystemColor.controlShadow);
		slider.setMaximum(7);
		slider.setMajorTickSpacing(1);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.setBounds(111, 39, 200, 39);
		MusicaInfantil.add(slider);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setValue(75);
		progressBar.setBounds(381, 27, 180, 14);
		MusicaInfantil.add(progressBar);
		
		JButton btnPlay = new JButton("Play");
		btnPlay.setBounds(381, 41, 81, 23);
		MusicaInfantil.add(btnPlay);
		
		JButton btnPause = new JButton("Pause");
		btnPause.setBounds(472, 41, 89, 23);
		MusicaInfantil.add(btnPause);
		
		JLabel label_1 = new JLabel("00:00");
		label_1.setBounds(350, 27, 46, 14);
		MusicaInfantil.add(label_1);
		
		JLabel label_2 = new JLabel("3:10");
		label_2.setBounds(563, 27, 46, 14);
		MusicaInfantil.add(label_2);
		
		JLabel lblBabySharkDance = new JLabel("Baby Shark Dance");
		lblBabySharkDance.setBounds(381, 14, 172, 14);
		MusicaInfantil.add(lblBabySharkDance);
		
		JLabel lblAoDelInfante = new JLabel("A\u00F1o del infante:");
		lblAoDelInfante.setBounds(10, 39, 108, 14);
		MusicaInfantil.add(lblAoDelInfante);
		
		JSpinner spinner = new JSpinner();
		spinner.setBounds(70, 95, 29, 20);
		MusicaInfantil.add(spinner);
		
		JLabel lblMinutos = new JLabel("Minutos:");
		lblMinutos.setBounds(10, 98, 56, 14);
		MusicaInfantil.add(lblMinutos);
	}
}
